# Dotfiles
Config files for:

- GIT
- Kitty
- VIM
- ZSH
- VSCODE
- [Powerlevel10k](https://github.com/romkatv/powerlevel10k#manual)

<br />



### Idea based on repo from:  https://github.com/nschurmann/configs.git
